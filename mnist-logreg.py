
# coding: utf-8

# In[7]:

__imp


# In[1]:

import numpy as np
import os
#os.environ['CUDARRAY_BACKEND'] = 'numpy'
import deeppy as dp
import matplotlib
import matplotlib.pyplot as plt


# Fetch MNIST data
dataset = dp.dataset.MNIST()
x_train, y_train, x_test, y_test = dataset.data(dp_dtypes=True)

# Bring images to BCHW format
x_train = x_train[:, np.newaxis, :, :]
x_test = x_test[:, np.newaxis, :, :]

# Normalize pixel intensities
scaler = dp.StandardScaler()
x_train = scaler.fit_transform(x_train)
x_test = scaler.transform(x_test)


# In[2]:

x_train = x_train[(y_train==0) | (y_train==1)]


# In[3]:

y_train = y_train[(y_train==0) | (y_train==1)]
# #y_train[y_train==4] = 1


# In[4]:

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logging.debug("test")


# # SAG

# In[10]:

from sgd import StochasticGradientDescent
from learn_rule_sag import SAG
from idx_input import SupervisedIdxInput

# Prepare network inputs
N_obj = 20000
batch_size = 1
train_input = SupervisedIdxInput(x_train[:N_obj], y_train[:N_obj], batch_size=batch_size)
test_input = dp.Input(x_test)


weight_gain_fc = 1.84
weight_decay_fc = 0.0
net = dp.NeuralNetwork(
    layers=[
        dp.Flatten(),
        dp.FullyConnected(
            n_out=2,
            weights=dp.Parameter(dp.AutoFiller(weight_gain_fc),
                                 weight_decay=weight_decay_fc),
        ),
    ],
    loss=dp.SoftmaxCrossEntropy(),
)
net._setup(**train_input.shapes)

# Train network
lrsag = SAG(x_num = N_obj,model = net, L = 10000, use_sag = True)

trainer = StochasticGradientDescent(max_epochs=10, learn_rule=lrsag)
a = trainer.train(net, train_input,adapt_L = True)


# In[11]:

cost_array_sag = np.array(trainer.cost)


# # SGD

# In[19]:

from sgd import StochasticGradientDescent
from learn_rule_sag import SAG
from idx_input import SupervisedIdxInput

# Prepare network inputs
N_obj = 20000
batch_size = 1
train_input = SupervisedIdxInput(x_train[:N_obj], y_train[:N_obj], batch_size=batch_size)
test_input = dp.Input(x_test)


weight_gain_fc = 1.84
weight_decay_fc = 0.0
net = dp.NeuralNetwork(
    layers=[
        dp.Flatten(),
        dp.FullyConnected(
            n_out=2,
            weights=dp.Parameter(dp.AutoFiller(weight_gain_fc),
                                 weight_decay=weight_decay_fc),
        ),
    ],
    loss=dp.SoftmaxCrossEntropy(),
)
net._setup(**train_input.shapes)

# Train network
lrsag = SAG(x_num = N_obj,model = net, L = 10000, use_sag = False)

trainer = StochasticGradientDescent(max_epochs=10, learn_rule=lrsag)
a = trainer.train(net, train_input,adapt_L = True)


# In[20]:

cost_array_sgd = np.array(trainer.cost)


# In[21]:

hdump([cost_array_sgd, cost_array_sag], '../log_sgd_sag_logreg.h')


# In[31]:

plt.plot(pd.rolling_mean(cost_array_sag,1000),label = 'sag')
plt.plot(pd.rolling_mean(cost_array_sgd,1000),label = 'sgd')
plt.legend()


# In[27]:

import matplotlib as mpl

mpl.rcParams['axes.color_cycle'] =['#ff0000', '#0000ff',   '#00ffff','#ffA300', '#00ff00', 
     '#ff00ff', '#990000', '#009999', '#999900', '#009900', '#009999']
from matplotlib import rc
rc('font', size=16)
rc('font',**{'family':'serif','serif':['Computer Modern']})
rc('text', usetex=True)
rc('figure', figsize=(16, 14))
rc('axes', linewidth=.5)
rc('lines', linewidth=1.75)

