
# coding: utf-8

# In[8]:

import numpy as np
import os
#os.environ['CUDARRAY_BACKEND'] = 'numpy'
import deeppy as dp
import matplotlib
import matplotlib.pyplot as plt


# Fetch MNIST data
dataset = dp.dataset.MNIST()
x_train, y_train, x_test, y_test = dataset.data(dp_dtypes=True)

# Bring images to BCHW format
x_train = x_train[:, np.newaxis, :, :]
x_test = x_test[:, np.newaxis, :, :]

# Normalize pixel intensities
scaler = dp.StandardScaler()
x_train = scaler.fit_transform(x_train)
x_test = scaler.transform(x_test)


# In[3]:

x_train = x_train[(y_train==0) | (y_train==1)]


# In[4]:

y_train = y_train[(y_train==0) | (y_train==1)]
# #y_train[y_train==4] = 1


# In[5]:

import logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logging.debug("test")


# In[7]:

from sgd import StochasticGradientDescent
from learn_rule_sag import SAG
from idx_input import SupervisedIdxInput

# Prepare network inputs
N_obj = 20000
batch_size = 1
train_input = SupervisedIdxInput(x_train[:N_obj], y_train[:N_obj], batch_size=batch_size)
test_input = dp.Input(x_test)


weight_gain_fc = 1.84
weight_decay_fc = 0.0
net = dp.NeuralNetwork(
    layers=[
        dp.Flatten(),
#         dp.FullyConnected(
#             n_out=20,
#             weights=dp.Parameter(dp.AutoFiller(weight_gain_fc),
#                                  weight_decay=weight_decay_fc),
#         ),
#         dp.ReLU(),
        dp.FullyConnected(
            n_out=2,
            weights=dp.Parameter(dp.AutoFiller(weight_gain_fc),
                                 weight_decay=weight_decay_fc),
        ),
    ],
    loss=dp.SoftmaxCrossEntropy(),
)
net._setup(**train_input.shapes)

all_params = [layer._params for layer in net.layers if isinstance(layer, dp.base.ParamMixin)]
import itertools
param_num = sum([x.grad_array.shape[-1] for x in list(itertools.chain.from_iterable(all_params))])

def dummy_old_grad(obj_idx, new_grad):
    rank = 500
    old_grad = dummy_old_grad.g[obj_idx, :].copy()
    dummy_old_grad.g[obj_idx, :] = new_grad
#     u, s, v = np.linalg.svd(g)
#     u = u[:, :rank]
#     s = s[:rank]
#     v = v[:rank, :]
#     dummy_old_grad.g = u.dot(np.diag(s)).dot(v)
    return old_grad

dummy_old_grad.g = np.zeros((N_obj,param_num))

# Train network
lrsag = SAG(x_num = N_obj,model = net, L = 10000, use_sag = True)

trainer = StochasticGradientDescent(max_epochs=10, learn_rule=lrsag)
a = trainer.train(net, train_input,adapt_L = True)


# In[ ]:




# In[3]:

__imp


# In[14]:




# In[95]:

__imp


# In[107]:

cost_array = np.array(trainer.cost)
plt.plot(pd.rolling_mean(cost_array,100))


# In[15]:

all_params = [layer._params for layer in net.layers if isinstance(layer, dp.base.ParamMixin)]
import itertools
list(itertools.chain.from_iterable(all_params))[2].array


# In[68]:

all_params = [layer._params for layer in net.layers if isinstance(layer, dp.base.ParamMixin)]
import itertools
list(itertools.chain.from_iterable(all_params))


# In[75]:

list(itertools.chain.from_iterable(all_params))[1].array


# In[80]:

import cudarray as ca
a = ca.ones(1)
ca.copyto()


# In[ ]:

ca.copyto()


# In[ ]:

all_params = [layer._params for layer in net.layers if isinstance(layer, dp.base.ParamMixin)]


# In[43]:

net.layers[1].weights.array


# In[49]:

net2.layers[3].weights.array[0] *=0


# In[35]:

import copy


# In[45]:

net2 = copy.deepcopy(net)


# In[ ]:

l.weights.grad_array.shape


# In[34]:

net_deep_copy()
net.bprop_untilcopy()


# In[7]:

import asgd


# In[ ]:

asgd.naive_asgd.


# In[5]:

sgd


# In[ ]:

INFO:sgd:SGD: Model contains 7850 parameters.
INFO:sgd:SGD: 11765 gradient updates per epoch.
INFO:sgd:SGD: 0, cost = 0.458890.
INFO:sgd:SGD: 1000, cost = 4.417035.
INFO:sgd:SGD: 2000, cost = 2.626298.
INFO:sgd:SGD: 3000, cost = 2.724252.
INFO:sgd:SGD: 4000, cost = 4.028182.
INFO:sgd:SGD: 5000, cost = 5.343396.
INFO:sgd:SGD: 6000, cost = 1.258388.
INFO:sgd:SGD: 7000, cost = 3.010685.
INFO:sgd:SGD: 8000, cost = 4.121953.
INFO:sgd:SGD: 9000, cost = 2.973456.
INFO:sgd:SGD: 10000, cost = 1.959813.
INFO:sgd:SGD: 11000, cost = 0.451697.
INFO:sgd:======== epoch 1/5, cost 2.983933
INFO:sgd:SGD: 0, cost = 0.210459.
INFO:sgd:SGD: 1000, cost = 1.695387.
INFO:sgd:SGD: 2000, cost = 1.738432.
INFO:sgd:SGD: 3000, cost = 1.367933.
INFO:sgd:SGD: 4000, cost = 0.756829.
INFO:sgd:SGD: 5000, cost = 1.878646.
INFO:sgd:SGD: 6000, cost = 1.430874.
INFO:sgd:SGD: 7000, cost = 0.844486.
INFO:sgd:SGD: 8000, cost = 0.878055.
INFO:sgd:SGD: 9000, cost = 1.085068.
INFO:sgd:SGD: 10000, cost = 1.401724.
INFO:sgd:SGD: 11000, cost = 0.572834.
INFO:sgd:======== epoch 2/5, cost 1.292209
INFO:sgd:SGD: 0, cost = 0.242451.
INFO:sgd:SGD: 1000, cost = 0.945775.
INFO:sgd:SGD: 2000, cost = 0.640329.
INFO:sgd:SGD: 3000, cost = 0.685407.
INFO:sgd:SGD: 4000, cost = 0.545519.
INFO:sgd:SGD: 5000, cost = 0.614893.
INFO:sgd:SGD: 6000, cost = 0.697633.
INFO:sgd:SGD: 7000, cost = 0.435023.
INFO:sgd:SGD: 8000, cost = 0.552319.
INFO:sgd:SGD: 9000, cost = 0.227984.
INFO:sgd:SGD: 10000, cost = 0.573232.
INFO:sgd:SGD: 11000, cost = 0.468296.
INFO:sgd:======== epoch 3/5, cost 0.554686
INFO:sgd:SGD: 0, cost = 0.040424.
INFO:sgd:SGD: 1000, cost = 0.539330.
INFO:sgd:SGD: 2000, cost = 0.402364.
INFO:sgd:SGD: 3000, cost = 0.295983.
INFO:sgd:SGD: 4000, cost = 0.283400.
INFO:sgd:SGD: 5000, cost = 0.231612.
INFO:sgd:SGD: 6000, cost = 0.510239.
INFO:sgd:SGD: 7000, cost = 0.233825.
INFO:sgd:SGD: 8000, cost = 0.396255.
INFO:sgd:SGD: 9000, cost = 0.330065.
INFO:sgd:SGD: 10000, cost = 0.488871.
INFO:sgd:SGD: 11000, cost = 0.492779.
INFO:sgd:======== epoch 4/6, cost 0.360764
INFO:sgd:SGD: 0, cost = 0.028409.
INFO:sgd:SGD: 1000, cost = 0.420549.
INFO:sgd:SGD: 2000, cost = 0.310715.
INFO:sgd:SGD: 3000, cost = 0.532655.
INFO:sgd:SGD: 4000, cost = 0.231054.
INFO:sgd:SGD: 5000, cost = 0.244247.
INFO:sgd:SGD: 6000, cost = 0.241243.
INFO:sgd:SGD: 7000, cost = 0.301002.
INFO:sgd:SGD: 8000, cost = 0.323839.
INFO:sgd:SGD: 9000, cost = 0.387525.
INFO:sgd:SGD: 10000, cost = 0.294373.
INFO:sgd:SGD: 11000, cost = 0.187335.
INFO:sgd:======== epoch 5/7, cost 0.318993
INFO:sgd:SGD: 0, cost = 0.040776.
INFO:sgd:SGD: 1000, cost = 0.348480.
INFO:sgd:SGD: 2000, cost = 0.333066.
INFO:sgd:SGD: 3000, cost = 0.279392.
INFO:sgd:SGD: 4000, cost = 0.465469.
INFO:sgd:SGD: 5000, cost = 0.204539.
INFO:sgd:SGD: 6000, cost = 0.783240.
INFO:sgd:SGD: 7000, cost = 0.461821.
INFO:sgd:SGD: 8000, cost = 0.263990.
INFO:sgd:SGD: 9000, cost = 0.242737.
INFO:sgd:SGD: 10000, cost = 0.305832.
INFO:sgd:SGD: 11000, cost = 0.307616.
INFO:sgd:======== epoch 6/9, cost 0.291276
INFO:sgd:SGD: 0, cost = 0.194865.
INFO:sgd:SGD: 1000, cost = 0.515789.
INFO:sgd:SGD: 2000, cost = 0.258697.
INFO:sgd:SGD: 3000, cost = 0.183047.
INFO:sgd:SGD: 4000, cost = 0.164617.
INFO:sgd:SGD: 5000, cost = 0.682310.
INFO:sgd:SGD: 6000, cost = 0.129562.
INFO:sgd:SGD: 7000, cost = 0.401927.
INFO:sgd:SGD: 8000, cost = 0.258269.
INFO:sgd:SGD: 9000, cost = 0.279894.
INFO:sgd:SGD: 10000, cost = 0.191039.
INFO:sgd:SGD: 11000, cost = 0.423675.
INFO:sgd:======== epoch 7/9, cost 0.293093
INFO:sgd:SGD: 0, cost = 2.072477.
INFO:sgd:SGD: 1000, cost = 0.302095.
INFO:sgd:SGD: 2000, cost = 0.186857.
INFO:sgd:SGD: 3000, cost = 0.204608.
INFO:sgd:SGD: 4000, cost = 0.245559.
INFO:sgd:SGD: 5000, cost = 0.264163.
INFO:sgd:SGD: 6000, cost = 0.225029.
INFO:sgd:SGD: 7000, cost = 0.188798.
INFO:sgd:SGD: 8000, cost = 0.350346.
INFO:sgd:SGD: 9000, cost = 0.284401.
INFO:sgd:SGD: 10000, cost = 0.246687.
INFO:sgd:SGD: 11000, cost = 0.470493.
INFO:sgd:======== epoch 8/9, cost 0.290027
INFO:sgd:SGD: 0, cost = 0.114586.
INFO:sgd:SGD: 1000, cost = 0.242059.
INFO:sgd:SGD: 2000, cost = 0.236183.
INFO:sgd:SGD: 3000, cost = 0.200174.
INFO:sgd:SGD: 4000, cost = 0.327526.
INFO:sgd:SGD: 5000, cost = 0.184101.
INFO:sgd:SGD: 6000, cost = 0.416828.
INFO:sgd:SGD: 7000, cost = 0.198218.
INFO:sgd:SGD: 8000, cost = 0.296629.
INFO:sgd:SGD: 9000, cost = 0.235172.
INFO:sgd:SGD: 10000, cost = 0.233036.
INFO:sgd:SGD: 11000, cost = 0.382469.
INFO:sgd:======== epoch 9/13, cost 0.287628
INFO:sgd:SGD: 0, cost = 0.006652.
INFO:sgd:SGD: 1000, cost = 0.320464.
INFO:sgd:SGD: 2000, cost = 0.331575.
INFO:sgd:SGD: 3000, cost = 0.563375.
INFO:sgd:SGD: 4000, cost = 0.167224.
INFO:sgd:SGD: 5000, cost = 0.220077.
INFO:sgd:SGD: 6000, cost = 0.480035.
INFO:sgd:SGD: 7000, cost = 0.321976.
INFO:sgd:SGD: 8000, cost = 0.259819.
INFO:sgd:SGD: 9000, cost = 0.330385.
INFO:sgd:SGD: 10000, cost = 0.142308.
INFO:sgd:SGD: 11000, cost = 0.403927.
INFO:sgd:======== epoch 10/15, cost 0.282270
INFO:sgd:SGD: Stopped by max_epochs.


# In[112]:

0.25*(np.max(np.linalg.norm(x_train.reshape((60000,-1)),axis=1))+1)


# In[ ]:

INFO:sgd:SGD: Model contains 7850 parameters.
INFO:sgd:SGD: 20000 gradient updates per epoch.
INFO:sgd:SGD: 0, cost = 2.218066.
INFO:sgd:SGD: 1000, cost = 2.695839.
INFO:sgd:SGD: 2000, cost = 2.463513.
INFO:sgd:SGD: 3000, cost = 2.414884.
INFO:sgd:SGD: 4000, cost = 2.336373.
INFO:sgd:SGD: 5000, cost = 2.183285.
INFO:sgd:SGD: 6000, cost = 2.032248.
INFO:sgd:SGD: 7000, cost = 1.867176.
INFO:sgd:SGD: 8000, cost = 1.696344.
INFO:sgd:SGD: 9000, cost = 1.397496.
INFO:sgd:SGD: 10000, cost = 1.333949.
INFO:sgd:SGD: 11000, cost = 1.374232.
INFO:sgd:SGD: 12000, cost = 1.297172.
INFO:sgd:SGD: 13000, cost = 1.216552.
INFO:sgd:SGD: 14000, cost = 1.382218.
INFO:sgd:SGD: 15000, cost = 0.969789.
INFO:sgd:SGD: 16000, cost = 0.916342.
INFO:sgd:SGD: 17000, cost = 1.282511.
INFO:sgd:SGD: 18000, cost = 0.533705.
INFO:sgd:SGD: 19000, cost = 0.628567.
INFO:sgd:======== epoch 1/5, cost 1.572128
INFO:sgd:SGD: 0, cost = 2.166318.
INFO:sgd:SGD: 1000, cost = 0.582403.
INFO:sgd:SGD: 2000, cost = 0.684643.
INFO:sgd:SGD: 3000, cost = 0.428067.
INFO:sgd:SGD: 4000, cost = 0.386918.
INFO:sgd:SGD: 5000, cost = 0.535594.
INFO:sgd:SGD: 6000, cost = 0.420161.
INFO:sgd:SGD: 7000, cost = 0.676061.
INFO:sgd:SGD: 8000, cost = 0.612575.
INFO:sgd:SGD: 9000, cost = 0.496174.
INFO:sgd:SGD: 10000, cost = 0.690191.
INFO:sgd:SGD: 11000, cost = 0.863458.
INFO:sgd:SGD: 12000, cost = 0.754025.
INFO:sgd:SGD: 13000, cost = 0.804344.
INFO:sgd:SGD: 14000, cost = 1.449844.
INFO:sgd:SGD: 15000, cost = 0.463697.
INFO:sgd:SGD: 16000, cost = 0.468017.
INFO:sgd:SGD: 17000, cost = 0.949975.
INFO:sgd:SGD: 18000, cost = 0.262370.
INFO:sgd:SGD: 19000, cost = 0.372363.
INFO:sgd:======== epoch 2/5, cost 0.620700
INFO:sgd:SGD: 0, cost = 2.280579.
INFO:sgd:SGD: 1000, cost = 0.461465.
INFO:sgd:SGD: 2000, cost = 0.720628.
INFO:sgd:SGD: 3000, cost = 0.472169.
INFO:sgd:SGD: 4000, cost = 0.490910.
INFO:sgd:SGD: 5000, cost = 0.805294.
INFO:sgd:SGD: 6000, cost = 0.638910.
INFO:sgd:SGD: 7000, cost = 0.863758.
INFO:sgd:SGD: 8000, cost = 1.480307.
INFO:sgd:SGD: 9000, cost = 0.601672.
INFO:sgd:SGD: 10000, cost = 0.454915.
INFO:sgd:SGD: 11000, cost = 1.036458.
INFO:sgd:SGD: 12000, cost = 0.709317.
INFO:sgd:SGD: 13000, cost = 0.673329.
INFO:sgd:SGD: 14000, cost = 0.978068.
INFO:sgd:SGD: 15000, cost = 0.593450.
INFO:sgd:SGD: 16000, cost = 0.671145.
INFO:sgd:SGD: 17000, cost = 0.715100.
INFO:sgd:SGD: 18000, cost = 0.253789.
INFO:sgd:SGD: 19000, cost = 0.381105.
INFO:sgd:======== epoch 3/5, cost 0.669974
INFO:sgd:SGD: 0, cost = 0.230296.


# In[93]:

gg = [np.array(x[0]) for x in  a]
weight_matrix = np.hstack(gg)


# In[99]:

hdump(aa,'mat.h',compression='gzip')


# In[96]:

plt.plot(aa[:,0])


# In[71]:

plt.plot(aa[0,:])

