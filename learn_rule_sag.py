import numpy as np
import cudarray as ca
import deeppy as dp


class LearnRule(object):
    learn_rate = None

    def init_state(self, param):
        raise NotImplementedError()

    def step(self, param, state):
        raise NotImplementedError()


def ravel(x):
    return ca.reshape(x, (ca.helpers.prod(x.shape),))


import copy
import itertools


class SAG(LearnRule):

    def __init__(self, x_num,  model, L=1, use_sag=True):
        #self.learn_rate = learn_rate

        self.num_objects = x_num
        self.use_sag = use_sag
        self.counter = 0
        self.model = model
        self.model_temp = copy.deepcopy(self.model)
        self.L = L

    def init_state(self, param):
        print '!!! ', param.grad_array.shape

        self.weights_num = ca.helpers.prod(param.grad_array.shape)

        last_grad_matrix = ca.zeros([self.num_objects, self.weights_num])
        grad_sum = ca.zeros_like(param.grad_array)

        return last_grad_matrix, grad_sum

    # def adapt_alpha(self, x, grad):
    #     alpha = self.alpha
    #     for i in range(10):
    #         l = self.model.fprop(x - 1. / self.alpha * grad)
    #         print "D!D!D! ", l.shape
    #         r = self.model.fprop(x - 1. / self.alpha * grad)

    def adapt_L(self, x, y, grad_list):

        #model_temp = copy.deepcopy(self.model)

        # temp model
        temp_all_params = [
            layer._params for layer in self.model_temp.layers if isinstance(layer, dp.base.ParamMixin)]
        temp_param_list = list(itertools.chain.from_iterable(temp_all_params))

        # original model
        model_all_params = [
            layer._params for layer in self.model.layers if isinstance(layer, dp.base.ParamMixin)]
        model_param_list = list(
            itertools.chain.from_iterable(model_all_params))

        L = self.L / 4.

        all_w = np.concatenate([np.array(g) for g in grad_list])
        no = np.linalg.norm(all_w)**2

        if no < 10:
            self.L = min(self.L * 0.99, 1e8)
            return

        for i in range(10):

            L *= 2

            # create new net for eval and substitute weights
            triple = zip(temp_param_list, model_param_list, grad_list)
            for temp_param, model_param, g in triple:
                ca.copyto(temp_param.array, model_param.array - 1. / L *
                          ca.reshape(g, model_param.grad_array.shape))

            l = self.model_temp.loss.loss(self.model_temp.fprop(x), y)
            r = self.model.loss.loss(
                self.model.fprop(x), y) - 1. / L / 2 * no

            if np.array(l)[0] < np.array(r)[0]:
                break

        self.L = min(L, 1e8)

    def step(self, param, state, x_idx, x, epoch):

        last_grad_matrix, grad_sum = state

        # calculate step
        last_grad = ca.reshape(
            last_grad_matrix[x_idx, :], param.grad_array.shape)
        new_grad = param.grad()

        grad_sum += (new_grad - last_grad)

        step = grad_sum / \
            min(self.counter + 1, self.num_objects) / self.L

        if not self.use_sag:
            step = new_grad / self.L

        # update matrix
        last_grad_matrix[x_idx, :] = ravel(new_grad)

        penalty = param.penalty()
        if penalty is not None:
            step += penalty
        step *= -1
        param.step(step)
        self.counter = min(self.counter + 1, self.num_objects)


class RMSProp(LearnRule):

    def __init__(self, learn_rate, decay=0.9, eps=1e-8):
        self.learn_rate = learn_rate
        self.decay = decay
        self.eps = eps

    def init_state(self, param):
        last_step = ca.zeros_like(param.grad_array)
        return last_step

    def step(self, param, last_step):
        last_step *= self.decay
        step = param.grad()
        penalty = param.penalty()
        if penalty is not None:
            step -= penalty
        last_step += (1.0 - self.decay) * step**2
        scaling = ca.sqrt(last_step) + self.eps
        step *= -self.learn_rate
        step /= scaling
        param.step(step)


class Adam(LearnRule):

    def __init__(self, learn_rate, beta1=0.9, beta2=0.999, lambd=1 - 1e-8,
                 eps=1e-8):
        self.learn_rate = learn_rate
        self.beta1 = beta1
        self.beta2 = beta2
        self.lambd = lambd
        self.eps = eps

    def init_state(self, param):
        m = ca.zeros_like(param.grad_array)
        v = ca.zeros_like(param.grad_array)
        t = np.zeros(1, dtype=int)
        return m, v, t

    def step(self, param, state):
        m, v, t = state
        grad = param.grad()
        penalty = param.penalty()
        if penalty is not None:
            grad -= penalty
        t += 1
        t = int(t)
        beta1_t = self.beta1 * self.lambd**(t - 1)
        m *= beta1_t
        m += (1 - beta1_t) * grad
        v *= self.beta2
        v += (1 - self.beta2) * grad**2
        learn_rate = (self.learn_rate * (1 - self.beta2**t)**0.5 /
                      (1 - self.beta1**t))
        step = m / (ca.sqrt(v) + self.eps)
        step *= -learn_rate
        param.step(step)
